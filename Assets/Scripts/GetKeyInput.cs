﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GetKeyInput : MonoBehaviour
{
    public Text txtKeyCode;
    public Text txtScreenResolution;
    public Text txtKeyCommand;

    private int frameInterval = 15;
    private string keyCommand = "";

    /*
     * Joystick1Button0
     * Left, Right, Up, Down, Arrow
     * */

    void Start()
    {
        ClearUI();
        GetScreenResolution();
    }

    void ClearUI()
    {
        txtKeyCode.text = " - ";
        txtScreenResolution.text = " - ";
    }

    void GetScreenResolution()
    {
        txtScreenResolution.text = Screen.width + " : " + Screen.height;
    }

    void GetInput()
    {
        if ( Input.GetKeyDown ( KeyCode.Joystick1Button0 ) )
            keyCommand = "BUTTON";
        else if ( Input.GetKeyDown( KeyCode.LeftArrow ) )
            keyCommand = "Left";
        else if ( Input.GetKeyDown( KeyCode.RightArrow ) )
            keyCommand = "Right";
        else if ( Input.GetKeyDown( KeyCode.UpArrow ) )
            keyCommand = "Up";
        else if ( Input.GetKeyDown( KeyCode.DownArrow ) )
            keyCommand = "Down";
        else
        {
            keyCommand = "...";
        }
    }

    void CheckKey()
    {
        foreach ( KeyCode kcode in Enum.GetValues( typeof( KeyCode ) ) )
        {
            if ( Input.GetKey( kcode ) )
            {
                //Debug.Log( "KeyCode down: " + kcode );
                txtKeyCode.text = kcode.ToString();
            }
        }
    }

    void Update()
    {
        if ( Time.frameCount % frameInterval == 0 )
            CheckKey();

        GetInput();

        txtKeyCommand.text = keyCommand;
    }
}
